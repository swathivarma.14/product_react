import React, { useState, useEffect } from "react";
import "./Menu.css";
import { Container, Row, Col, Button } from "reactstrap";
import axios from "axios";
import SearchForm from "../SearchForm/SearchForm";
import ProductsList from "../ProductsList/ProductsList";
import OrderHistory from "../OrderHistory/OrderHistory";
import data from "../../getProducts.json";

const Menu = (props) => {

    const [showProductListing, setShowProductListing] = useState(true);
    const [showOrderHistory, setShowOrderHistory] = useState(false);
    const [products, setProducts] = useState();
    const [displayProducts, setDisplayProducts] = useState();

    const getProductOrders = async () => {
        const baseurl = "http://localhost:4000/product";
        axios.get(`${baseurl}`).then((response) => {
            setProducts(response.data);
        });
    }

    const setOrderHistoryValue = () => {
        getProductOrders();
        setShowOrderHistory(true);
        setShowProductListing(false);
    }

    const setProductListingValue = () => {
        setShowOrderHistory(false);
        setShowProductListing(true);
        setDisplayProducts(data);
    }

    useEffect(() => {
        setDisplayProducts(data);
    }, [])
    
    return (
        <>
            <Container>
                <Row>
                    <Col xs="6" className="display">
                        <Button onClick={setProductListingValue}>
                            Product Listing
                        </Button>
                    </Col>
                    <Col xs="6">
                        <Button onClick={setOrderHistoryValue}>
                            Order History
                        </Button>
                    </Col>
                </Row>
            </Container>
            
            <div>            
                {(showProductListing && 
                    <Row>
                        <Col sm="3">
                            <SearchForm
                                products={products}
                                setDisplayProducts={setDisplayProducts}
                                displayProducts={displayProducts}
                                data={data}
                            />
                        </Col>
                        <Col sm="9">
                        {(displayProducts && <ProductsList displayProducts={displayProducts} />)}
                        </Col>
                    </Row>
                )}
            </div>
            <div>            
                {((showOrderHistory && products) && 
                    <Row>
                        <OrderHistory
                            products={products}
                        />
                    </Row>
                )}
            </div>
        </>
    )
}

export default Menu;