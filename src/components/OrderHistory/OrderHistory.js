import React, { useState } from 'react';
import { Button, Table, Container } from "reactstrap";
import MyPagination from '../MyPagination/MyPagination';

const OrderHistory = (props) => {

  const { products } = props;

  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(5);

  // Get current items
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItem = products.slice(indexOfFirstItem, indexOfLastItem);

  // Change Page
  const paginate = (pageNumber) => setCurrentPage(pageNumber)

  return (
    
    <Container>   
        <Table striped bordered>
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Product Color</th>
                    <th>Order Status</th>
                    <th>Order Date Time </th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {currentItem.map((item, n) => (
                    <tr>
                        <td>{item.order_id}</td>
                        <td>{item.product_id}</td>
                        <td>{item.product_name}</td>
                        <td>{item.product_color}</td>
                        <td>{item.order_status}</td>
                        <td>{item.order_time}</td>
                        <td><Button className="setComplete" size="sm">Set Completed</Button></td>
                    </tr>
                ))}
            </tbody>
        </Table>
        <MyPagination
            itemsPerPage={itemsPerPage}
            totalItems={products.length}
            paginate={paginate}
        />
    </Container>

  )
}

export default OrderHistory;
