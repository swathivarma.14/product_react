import React, { useState } from "react";
import "./SearchForm.css";
import { Container, Form, FormGroup, Label, Input, Button } from "reactstrap";

    const SearchForm = (props) => {

    const { data, setDisplayProducts } = props;

    const [productName, setProductName] = useState("");
    const [category, setCategory] = useState("");
    const [brand, setBrand] = useState("");
    const [color, setColor] = useState("");

    const onSearch = () => {

        if (productName === "" && category === "" && brand === "" && color === "") {
            return data;
        } else {
            const searchResult = data.filter(val => {
                return val.product_category.toLowerCase().includes(category.toLowerCase())
                && val.product_color.toLowerCase().includes(color.toLowerCase())
                && val.product_brand.toLowerCase().includes(brand.toLowerCase())
                && val.product_name.toLowerCase().includes(productName.toLowerCase())
            });
            setDisplayProducts(searchResult);
        }
    }

    const onClear = () => {
        setProductName("");
        setCategory("");
        setBrand("");
        setColor("");
        setDisplayProducts(data);
    }
    
    return (
        <Container>
            <h1>Search Filter</h1>
            <Form>
                <FormGroup>
                    <Label for="productName">
                        Product Name
                    </Label>
                    <Input
                    id="productName"
                    name="productName"
                    placeholder="Enter Product Name"
                    value={productName}
                    onChange={(e) => setProductName(e.target.value)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="category">
                        Category
                    </Label>
                    <Input
                    id="category"
                    name="category"
                    placeholder="Select Category Dropdown"
                    type="select"
                    value={category}
                    onChange={(e) => setCategory(e.target.value)}
                    >
                        <option value="">Select Category Dropdown</option>
                        <option>SmartPhones</option>
                        <option>Tablets</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="brand">
                        Brand
                    </Label>
                    <Input
                    id="brand"
                    name="brand"
                    type="select"
                    value={brand}
                    onChange={(e) => setBrand(e.target.value)}
                    >
                        <option value="">Select Brand Dropdown</option>
                        <option>Samsung</option>
                        <option>Apple</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="color">
                        Color
                    </Label>
                    <Input
                    id="color"
                    name="color"
                    type="select"
                    value={color}
                    onChange={(e) => setColor(e.target.value)}
                    >
                        <option value="">Select Color Dropdown</option>
                        <option>Green</option>
                        <option>Blue</option>
                        <option>Black</option>
                        <option>White</option>
                        <option>Grey</option>
                        <option>Silver</option>
                    </Input>
                </FormGroup>
                <Button className="search" onClick={onSearch}>Search</Button>
                <Button className="clear" onClick={onClear}>Clear Filter</Button>
            </Form>
        </Container>
    )
}

export default SearchForm;

