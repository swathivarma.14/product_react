import axios from "axios";
import React from "react";
import { Container, Button } from "reactstrap";
import './ProductsList.css';

const ProductsList = (props) => {

    const { displayProducts } = props;

    const createOrder = async (item) => {
        const baseurl = "http://localhost:4000/product";
        const resp = await axios.post(baseurl, {
            product_id: item.product_id,
            product_name: item.product_name,
            product_color: item.product_color,
            order_status: "Open",
            product_category: item.product_category,
            product_brand: item.product_brand,
            product_price: item.product_price
        });
       if (resp.status === 201) {
           alert('Order created successfully')
       } else {
           alert('Order creation failed');
       }
    }
    
    return (
        <Container className="container">
            {displayProducts.map((item, key) => (
                <div className="card" key={key}>
                    <div className="image">
                        <img src="/assets/png/mobile1.png" alt="mobile" height="150px" width="150px"/>
                    </div>
                    <div className="text">
                        <h5>{item.product_name}</h5>
                        <h5>{item.product_price}</h5>
                        <Button onClick={() => createOrder(item)}>Place Order</Button>
                    </div>
                </div>
            ))}
        </Container>
    )
}

export default ProductsList;